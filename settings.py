# -*- encoding: utf-8 -*-
from pathlib import Path
from datetime import date
from decimal import Decimal

from django.urls import reverse_lazy

from debconf.settings import *

TIME_ZONE = 'Asia/Kolkata'
USE_L10N = False
TIME_FORMAT = 'G:i'
SHORT_DATE_FORMAT = 'Y-m-d'
DATETIME_FORMAT = 'Y-m-d H:i:s'
SHORT_DATETIME_FORMAT = 'Y-m-d H:i'

try:
    from localsettings import *
except ImportError:
    pass

root = Path(__file__).parent

INSTALLED_APPS = (
    'dc23',
    'news',
    'volunteers',
) + INSTALLED_APPS

STATIC_ROOT = str(root / 'localstatic/')

STATICFILES_DIRS = (
    str(root / 'static'),
)

STATICFILES_STORAGE = (
    'django.contrib.staticfiles.storage.ManifestStaticFilesStorage')

TEMPLATES[0]['DIRS'] = TEMPLATES[0]['DIRS'] + (
    str(root / 'templates'),
)

TEMPLATES[0]['OPTIONS']['context_processors'] += (
    'dc23.context_processors.expose_wafer_talks',
    'dc23.context_processors.is_it_debconf'
)

WAFER_MENUS += (
    {
        'menu': 'about',
        'label': 'About',
        'items': [
            {
                'menu': 'coc',
                'label': 'Code of Conduct',
                'url': reverse_lazy('wafer_page', args=('about/coc',)),
            },
            {
                'menu': 'debconf',
                'label': 'DebConf',
                'url': reverse_lazy('wafer_page', args=('about/debconf',)),
            },
            {
                'menu': 'debcamp',
                'label': 'DebCamp',
                'url': reverse_lazy('wafer_page', args=('about/debcamp',)),
            },
            {
                'menu': 'debian',
                'label': 'Debian',
                'url': reverse_lazy('wafer_page', args=('about/debian',)),
            },
            {
                'menu': 'accommodation',
                'label': 'Accommodation',
                'url': reverse_lazy('wafer_page', args=('about/accommodation',)),
            },
            {
                'menu': 'bursaries',
                'label': 'Bursaries',
                'url': reverse_lazy('wafer_page', args=('about/bursaries',)),
            },
            {
                'menu': 'child-care',
                'label': 'Child care',
                'url': reverse_lazy('wafer_page', args=('about/childcare',)),
            },
            {
                'menu': 'COVID-19',
                'label': 'COVID-19',
                'url': reverse_lazy('wafer_page', args=('about/covid19',)),
            },
            {
                'menu': 'faq',
                'label': 'FAQ',
                'url': 'https://wiki.debian.org/DebConf/23/Faq',
            },
            {
                'menu': 'Kochi',
                'label': 'Kochi',
                'url': reverse_lazy('wafer_page', args=('about/kochi',)),
            },
            {
                'menu': 'registration_information',
                'label': 'Registration Information',
                'url': reverse_lazy('wafer_page', args=('about/registration',)),
            },
            {
                'menu': 'venue',
                'label': 'Venue',
                'url': reverse_lazy('wafer_page', args=('about/venue',)),
            },
            {
                'menu': 'visas',
                'label': 'Visas',
                'url': reverse_lazy('wafer_page', args=('about/visas',)),
            },
            {
                'menu': 'contact_link',
                'label': 'Contact us',
                'url': reverse_lazy('wafer_page', args=('contact',)),
            },
        ],
    },
    {
        'menu': 'sponsors_index',
        'label': 'Sponsors',
        'items': [
            {
                'menu': 'become_sponsor',
                'label': 'Become a Sponsor',
                'url': reverse_lazy('wafer_page', args=('sponsors/become-a-sponsor',)),
            },
            {
                'menu': 'sponsors',
                'label': 'Our Sponsors',
                'url': reverse_lazy('wafer_sponsors'),
            },
        ],
    },
    {
        'menu': 'schedule_index',
        'label': 'Schedule',
        'items': [
            {
                'menu': 'cfp',
                'label': 'Call for Proposals',
                'url': reverse_lazy('wafer_page', args=('cfp',)),
            },
            {
                'menu': 'confirmed_talks',
                'label': 'Confirmed Talks',
                'url': reverse_lazy('wafer_users_talks'),
            },
            {
                'menu': 'important-dates',
                'label': 'Important Dates',
                'url': reverse_lazy('wafer_page', args=('schedule/important-dates',)),
            },
            {
                'menu': 'mobile_schedule',
                'label': 'Mobile-Friendly Schedule',
                'url': reverse_lazy('wafer_page', args=('schedule/mobile',)),
            },
            {
                'menu': 'schedule',
                'label': 'Schedule',
                'url': reverse_lazy('wafer_full_schedule'),
            },
        ],
    },
    {
        'menu': 'wiki_link',
        'label': 'Wiki',
        'url': 'https://wiki.debian.org/DebConf/23',
    },
    {
        'menu': 'volunteers',
        'label': 'Volunteer!',
        'url': reverse_lazy('wafer_tasks'),
    },
)

WAFER_DYNAMIC_MENUS = ()

ROOT_URLCONF = 'urls'

CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_FAIL_SILENTLY = not DEBUG

DEFAULT_FROM_EMAIL = 'registration@debconf.org'
REGISTRATION_DEFAULT_FROM_EMAIL = 'noreply@debconf.org'

WAFER_REGISTRATION_MODE = 'custom'
WAFER_USER_IS_REGISTERED = 'register.models.user_is_registered'

WAFER_VIDEO_REVIEWER = False
WAFER_VIDEO_LICENSE = 'DebConf Video License (MIT-like)'
WAFER_VIDEO_LICENSE_URL = 'https://video.debian.net/LICENSE'

WAFER_PUBLIC_ATTENDEE_LIST = False

WAFER_TALK_LANGUAGES = (
# If we support multiple talk languages, list them here:
#    ("en", "English"),
)

WAFER_CONFERENCE_ACRONYM = 'debconf23'
DEBCONF_ONLINE = False
DEBCONF_CITY = 'Kochi'
DEBCONF_NAME = 'DebConf 23'
DEBCONF_CONFIRMATION_DEADLINE = date(2023, 8, 12)
DEBCONF_BURSARY_DEADLINE = date(2023, 7, 1)
DEBCONF_BURSARY_ACCEPTANCE_DEADLINE = date(2023, 8, 6)
DEBCONF_BILLING_CURRENCY = 'EUR'
DEBCONF_BILLING_CURRENCY_SYMBOL = '€'
DEBCONF_BURSARY_CURRENCY = 'USD'
DEBCONF_BURSARY_CURRENCY_SYMBOL = '$'
DEBCONF_LOCAL_CURRENCY = 'INR'
DEBCONF_LOCAL_CURRENCY_SYMBOL = '₹'
DEBCONF_LOCAL_CURRENCY_RATE = Decimal('85.00')
DEBCONF_REVIEW_FREE_ATTENDEES = True
DEBCONF_BREAKFAST = True
DEBCONF_INVOICE_ADDRESS = """\
c/o Association Debian France
1 passage de Bretagne
Boîte 084
93110
Rosny-Sous-Bois
France"""

DEBCONF_DATES = (
    # Conference part, start date, end date
    ('DebCamp', date(2023, 9, 3), date(2023, 9, 9)),
    ('DebConf', date(2023, 9, 10), date(2023, 9, 17)),
)
DEBCONF_CONFERENCE_DINNER_DAY = date(2023, 9, 14)
DEBCONF_SKIPPED_MEALS = (
    ('breakfast', date(2023, 9, 3)),
)

VOLUNTEERS_FIRST_DAY = date(2023, 9, 3)
VOLUNTEERS_LAST_DAY = date(2023, 9, 17)

DEBCONF_T_SHIRT_SIZES = (
    ('', 'No T-shirt'),
    ('', '—'),
    ('s', 'S - Small'),
    ('m', 'M - Medium'),
    ('l', 'L - Large'),
    ('xl', 'XL - Extra Large'),
    ('2xl', '2XL - Extra Large 2'),
    ('3xl', '3XL - Extra Large 3'),
    ('4xl', '4XL - Extra Large 4'),
    ('5xl', '5XL - Extra Large 5'),
    ('s_f', 'S - Small, Fitted cut'),
    ('m_f', 'M - Medium, Fitted cut'),
    ('l_f', 'L - Large, Fitted cut'),
    ('xl_f', 'XL - Extra Large, Fitted cut'),
    ('2xl_f', '2XL - Extra Large 2, Fitted cut'),
)
DEBCONF_SHOE_SIZES = ()

INVOICE_PREFIX = 'DC23-'

PRICES = {
    'fee': {
        '': {
            'name': 'Regular',
            'price': 0,
        },
        'pro': {
            'name': 'Professional',
            'price': 200,
        },
        'corp': {
            'name': 'Corporate',
            'price': 500,
        },
    },
    'meal': {
        'breakfast': {
            'price': 12,
        },
        'lunch': {
            'price': 18,
        },
        'dinner': {
            'price': 18,
        },
        'conference_dinner': {
            'price': 25,
        },
    },
    'accomm': {
        'shared': {
            'description': 'On-site shared room',
            'bursary': True,
        },
        'self-paid': {
            'description': 'Self-paid on-site accommodation (includes on-site meals)',
            'included_meals': {'breakfast', 'lunch', 'dinner'},
            'paid_separately': True,
        },
    },
}

DEBCONF_TALK_PROVISION_URLS = {
    'etherpad': {
        'pattern': 'https://pad.dc23.debconf.org/p/{id}-{slug:.45}',
        'public': True,
    },
}

DEBCONF_VENUE_IRC_CHANNELS = ["#debconf-{name}", "#debconf"]
DEBCONF_VENUE_STREAM_HLS_URL = "https://onsite.live.debconf.org/live/{name}.m3u8"
DEBCONF_VENUE_STREAM_RTMP_URL = "rtmp://onsite.live.debconf.org/front/{name}_{quality}"

PAGE_DIR = '%s/' % (root / 'pages')
NEWS_DIR = '%s/' % (root / 'news' / 'stories')
SPONSORS_DIR = '%s/' % (root / 'sponsors')

TRACKS_FILE = str(root / 'tracks.yml')
TALK_TYPES_FILE = str(root / 'talk_types.yml')

BAKERY_VIEWS += (
    'news.views.NewsItemView',
    'news.views.NewsFeedView',
    'news.views.NewsRSSView',
    'news.views.NewsAtomView',
)
BUILD_DIR = '%s/' % (root / 'static_mirror')
