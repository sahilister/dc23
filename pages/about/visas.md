---
name: Visas
---

# Indian Visa

- [Official website](https://indianvisaonline.gov.in/)

<div class="alert alert-dark border border-warning" role="alert">
  <i class="fa fa-warning" aria-hidden="true"></i>
  <span class="font-weight-bold"> WARNING: </span>
  Beware of unauthentic/fraudulent websites. Make sure you follow the official
  government website for Visa application and related information.
</div>

All foreigners visiting India need a valid passport and Indian visa. Only
categories exempted under [bilateral arrangements](https://www.mea.gov.in/bvwa-menu.htm)
may not need a visa. For persons of Indian origin (all categories), the OCI card
is mandatory.

Visa applications can be done via two modes, **e-Visa** and **regular visa**.
e-Visa will be easier to apply for and faster (3-4
working days).

Citizens of 166 countries are eligible for e-Visa, the complete list of can be
found on the [e-Visa portal](https://indianvisaonline.gov.in/evisa). Those who
are ineligible for e-Visa should apply for regular visa with the local Indian
Mission/Embassy in their country.

India provides a "Visa on Arrival" only to the nationals of Japan, South Korea and
UAE (only for such UAE nationals who had earlier obtained a visa for India) and
are available at selected Airports (Delhi, Mumbai, Chennai, Kolkata, Bengaluru,
Hyderabad) only. For more info, please visit [here](https://indianvisaonline.gov.in/visa/visa-on-arrival.html).

Those travelling to India are also advised to go through instructions available
on the [official website of Bureau of Immigration](https://boi.gov.in).


## Applying for e-visa:

<div class="alert alert-info" role="info">
  <i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="font-weight-bold"> PLEASE NOTE: </span>
  Ensure that you read all the instructions and FAQs in the official website
  properly before applying.
</div>

**Official website:**

- [eVisa website](https://indianvisaonline.gov.in/evisa/)
- [Sample application form (eTourist visa)](https://indianvisaonline.gov.in/evisa/images/SampleForm.pdf)


#### Notes

- See [Conference Visa](#conference-visa) below.
- e-Visa is non-extendable, non-convertible & not valid for visiting
[Protected/Restricted and Cantonment Areas](https://www.mha.gov.in/PDF_Other/AnnexVII_01022018.pdf).
If you intend to visit Protected/Restricted/Cantonment areas, you would require
prior permission from the Civil Authority.

- For e-Tourist and e-Business visas, Applicants of the eligible
countries/territories may apply online minimum 4 days in advance of the date of
arrival.

- Persons holding eVisa will be allowed to enter into India only through the 29
designated international airports namely Ahmedabad, Amritsar, Bagdogra,
Bengaluru, Bhubaneshwar, Calicut, Chennai, Chandigarh, Cochin, Coimbatore,
Delhi, Gaya, Goa, Guwahati, Hyderabad, Jaipur, Kannur, Kolkata, Lucknow,
Madurai, Mangalore, Mumbai, Nagpur, Port Blair, Pune, Tiruchirapalli,
Trivandrum, Varanasi & Visakhapatnam, and 5 designated seaports (i.e. Cochin,
Goa, Mangalore, Chennai and Mumbai seaports). They may depart from any of the
Indian Immigration Check Posts (ICPs).

- e-Visa fee is Country/Territory specific. To know the fee applicable for
e-Tourist Visa on your Country/Territory please [click here](https://indianvisaonline.gov.in/evisa/images/Etourist_fee_final.pdf)
and for other e-Visa [click here](https://indianvisaonline.gov.in/evisa/images/eTV_revised_fee_final.pdf).

<div class="alert alert-info" role="alert">
  <i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;
  The e-Visa fee payment may take up to 2 hours to appear to be due to technical
  reasons/network delays. Before re-applying, the applicants are requested to
  wait for 2 hours for payment status updating, after final submission of the
  application form and payment of the fee.
</div>


## Applying for Regular/Paper visa through Indian Mission/Post

<div class="alert alert-info" role="info">
  <i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="font-weight-bold"> PLEASE NOTE: </span>
  Ensure that you read all the instructions and FAQs in the official website
  properly before applying.
</div>

**Official website:**

- [Regular/paper visa website](https://indianvisaonline.gov.in/visa/index.html)
- [Instructions](https://indianvisaonline.gov.in/visa/instruction.html)

#### Notes

- See [Conference Visa](#conference-visa) below.

- Applicants must fill and submit application form online. After online
submission, applicant must print the completed application form and sign and
submit the physical copy along with the supporting documents and the Passport
to the concerned Indian Visa Application Center (IVAC) or directly at the
Indian Mission on the scheduled appointed date.

- The applicants are requested to visit website of the Indian Mission concerned
for detailed information about Indian visa.

- The fee depends upon the type of applied for and its duration of visa.

- Upon receipt of the Visa Application through Indian Visa Application Center
or directly, the Indian Mission/ Post requires a minimum of three working days
to process the case and issue a visa depending upon the nationality and
excluding special cases.


## Conference visa

We suggest you apply for the e-Conference visa [instead of the tourist
visa](https://www.mha.gov.in/PDF_Other/ForeigD-FAQ-TVisa280710.pdf) for your
visit to India for DebConf23. Those applying for an e-Conference visa can also
do some tourism before and after the event.

The e-Conference visa is only valid for 30 days. If you plan on staying for more
than 30 days in India for tourism, you will have to apply for [regular
conference
visa](https://www.mha.gov.in/sites/default/files/2022-07/ForeigD-FAQs-on-ConferenceVisa.pdf)
which can combine tourism for up to 6 months along with attending the
conference.

To apply for either e-Conference visa or regular conference visa, you will
require invitation from the organizer and Political clearance from Ministry
of External Affairs (MEA), Government of India. Please contact [visa@debconf.org][]
(PGP: [`7E78 2DB5 3C92 93F9 B5FE  9DD9 A193 CADE 502F B448`]({% static "visa-pgp-key.asc" %}))
for both these documents.

[visa@debconf.org]: mailto:visa@debconf.org

We can provide you a visa invitation letter once your registration is confirmed.
We will have to mention who will pay for your expenses in the invitation
letter, so if you are applying for a bursary we will have to wait for bursary
approval before we can send you the visa invitation. If your expenses are covered by someone else, like your employer, then mention that in your email, so we can include that information in your invite letter.

For us to send visa invitation letters, please email us your:

1. Full name, as it appears on your passport
1. Passport number
1. Date of birth
1. Place (country) of birth
1. Nationality
1. Home Address
1. Photos of page(s) of your passport with personal information, personal photograph and passport number.

The invitation letter will be provided by Infopark Kochi.
The information above, including the passport scans, will be shared with
them.

The visa invitation letter process will take us at least three weeks,
please remember to request as early as possible. We have to submit the list
of participants who need visa to Ministry of External Affairs and get Political
clearance from Ministry of External Affairs (MEA), Government of India (this
can take up to two weeks). Getting signature from Infopark in the invitation
letter also takes time (this is processed in multiple batches).

**Note:** We will be able to provide you invitation letter only if you send
passport details by 1st August.

<div class="alert alert-info" role="alert">
  <i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;
  For more information and support relating to visa, please email the visa team
  <span class="font-weight-bold">visa[at]debconf[dot]org</span>.
</div>

### Completing the eConference visa form

The form is quite long, expect to spend at least half an hour on it.

Some tips to help you navigate the visa form:

* If the form gets stuck, try closing it and resuming the application.
* If you need to go back, you can save and exit, then resume from the
  first page by using your temporary application ID.
* *Visible identification marks:* refers to tattoos and visible
  scarring. You can put "NA" in the field.
* *Designation* refers to a job title.
* Prepare a square photograph, interface will make you crop out a square
  from it.

#### Conference Details:

* Name of the conference: DebConf
* Start Date: 03/09/2023
* End Date: 17/09/2023
* Address: Infopark Park Centre, Kakkanad, Kochi
* State: Kerala
* District: Ernakulam
* Pincode: 682042
* Name of Organizer: InfoPark Kochi
* Address: Infopark Park Centre, Kakkanad, 682042, Kerala
* Phone No: +91 484 2415217
* Email Id: info@infopark.in

#### Reference:

The visa team will provide the contact address of a local team member to
be used as a reference along with invitation letter and political
clearance from MEA.

#### Documents:

* Invitation from organizer: Will be provided by the visa team.
* Political clearance from MEA: Will be provided by the visa team.
