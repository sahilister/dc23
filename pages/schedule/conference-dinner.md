---
name: Conference Dinner
---

The conference dinner will be held at the Alegria Hotel at 8pm.

Buses will collect us at the venue at 7:30pm.

Return buses will be provided, schedule to be defined.
