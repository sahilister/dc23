---
title: Proxmox Platinum Sponsor of DebConf23
---

We are pleased to announce that [Proxmox](https://www.proxmox.com) has committed
to sponsor [DebConf23](https://debconf23.debconf.org) as Platinum Sponsor.

[Proxmox](https://www.proxmox.com) develops powerful, yet easy-to-use
open-source server software. The product portfolio from Proxmox, including
server virtualization, backup, and email security, helps companies of any size,
sector, or industry to simplify their IT infrastructures. The Proxmox solutions
are based on the great Debian platform, and we are happy that we can give back
to the community by sponsoring DebConf23.

With this commitment as Platinum Sponsor, Proxmox is contributing to make
possible our annual conference, and directly supporting the progress of Debian
and Free Software, helping to strengthen the community that continues to
collaborate on Debian projects throughout the rest of the year.

Thank you very much Proxmox, for your support of DebConf23!



## Become a sponsor too!

[DebConf23](https://debconf23.debconf.org) **will take place from September 10th
to 17th, 2023 in Kochi, India**, and will be preceded by DebCamp, from September
3rd to 9th.

And DebConf23 is still accepting sponsors! Interested companies and
organizations may contact the DebConf team through
[sponsors@debconf.org](mailto:sponsors@debconf.org), and visit the DebConf23
website at
[https://debconf23.debconf.org/sponsors/become-a-sponsor/](https://debconf23.debconf.org/sponsors/become-a-sponsor/)
.
